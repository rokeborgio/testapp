package com.roke.test1.ui.main

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.roke.test1.model.ImageListModel
import java.io.InputStream


class MainViewModel : ViewModel() {

    val imageList = MutableLiveData<ImageListModel>()

    fun getImageList(context: Context){
        val gson = Gson()
        imageList.value = gson.fromJson(loadJsonFromFile(context), ImageListModel::class.java)
    }

    /**
     * read data from json file in assets
     * @return string of data in assets
     */
    fun loadJsonFromFile(context: Context): String {
        try {
            val input_stream: InputStream = context.assets.open("data.json")
            val input_string = input_stream.bufferedReader().use { it.readText() }
            return input_string
        } catch (e: Exception) {
            Log.d("File reading exception", e.toString())
        }
        return ""
    }
}