package com.roke.test1.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.roke.test1.adapter.ImageDetailAdapter
import com.roke.test1.adapter.ImageListAdapter
import com.roke.test1.databinding.DetailFragmentBinding
import com.roke.test1.util.Constants.SELECTED_POSITION

class DetailFragment : Fragment() {

    private lateinit var binding: DetailFragmentBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DetailFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        viewModel.imageList.observe(viewLifecycleOwner, Observer {
            binding.pagerImgDetail.adapter = ImageDetailAdapter(it)
            binding.pagerImgDetail.setCurrentItem(requireArguments().getInt(SELECTED_POSITION, 0), false)
        })
    }
}