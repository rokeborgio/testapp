package com.roke.test1.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.roke.test1.R
import com.roke.test1.model.ImageListModel
import com.roke.test1.util.ConnectionCheck
import com.roke.test1.util.toast

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MainViewModel
    var imageListModel: ImageListModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.imageList.observe(this, Observer {
            imageListModel = it
        })
        getData()
    }

    private fun getData() {
        val connectionLiveData = ConnectionCheck(this)
        connectionLiveData.observe(this) { isNetworkAvailable ->
            if (!isNetworkAvailable)
                toast(getString(R.string.please_check_your_internet_connection))
            if (isNetworkAvailable && imageListModel == null) {
                viewModel.getImageList(this)
            }
        }
    }
}