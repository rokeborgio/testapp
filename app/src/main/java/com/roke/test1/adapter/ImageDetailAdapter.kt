package com.roke.test1.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.roke.test1.R
import com.roke.test1.databinding.ItemImageDetailBinding
import com.roke.test1.model.ImageListModel

class ImageDetailAdapter(val imageList: ImageListModel) :
    RecyclerView.Adapter<ImageDetailAdapter.ImageDetailViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageDetailViewHolder =
        ImageDetailViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_image_detail, parent, false
            )
        )

    override fun getItemCount() = imageList.size
    override fun onBindViewHolder(holder: ImageDetailViewHolder, position: Int) {
        holder.itemImageDetailBinding.imageListModelItem = imageList[position]
        Glide.with(holder.itemView.context)
            .load(imageList[position].url)
            .into((holder.itemImageDetailBinding.imgDetail))
    }

    class ImageDetailViewHolder(val itemImageDetailBinding: ItemImageDetailBinding) :
        RecyclerView.ViewHolder(itemImageDetailBinding.root)

}