package com.roke.test1.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.roke.test1.R
import com.roke.test1.databinding.ItemListImageBinding
import com.roke.test1.model.ImageListModel
import com.roke.test1.util.Constants.SELECTED_POSITION

class ImageListAdapter(val imageList: ImageListModel) :
    RecyclerView.Adapter<ImageListAdapter.ImageListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageListViewHolder =
        ImageListViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_list_image, parent, false
            )
        )

    override fun getItemCount() = imageList.size

    override fun onBindViewHolder(holder: ImageListViewHolder, position: Int) {
        Glide.with(holder.itemView.context)
            .load(imageList[position].url)
            .fitCenter()
            .into((holder.itemGridImageBinding.imgGrid))

        holder.itemView.setOnClickListener {
            val navController = Navigation.findNavController(it)
            val bundle = Bundle()
            bundle.putInt(SELECTED_POSITION, position)
            navController.navigate(
                R.id.action_home_to_detail,
                bundle
            )
        }
    }

    class ImageListViewHolder(val itemGridImageBinding: ItemListImageBinding) :
        RecyclerView.ViewHolder(itemGridImageBinding.root)
}